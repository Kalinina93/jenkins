#!/usr/bin/env python3
# -*- coding: utf-8 -*- 

import time
from selenium import webdriver
from selenium.webdriver.common.by import By
import logging
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service

chrome_options = Options()

chrome_options.add_argument("--disable-extensions")
chrome_options.add_argument("--no-sandbox")
chrome_options.add_argument("--headless")


class TestWebsite:

    def test_login_and_find_elem(self):
        """Login on website"""
        driver = webdriver.Chrome("/home/natasha/PycharmProjects/Jenkins/chromedriver", chrome_options=chrome_options)
        driver.get("https://www.saucedemo.com")

        driver.find_element(By.ID, "user-name").send_keys("standard_user")
        driver.find_element(By.NAME, "password").send_keys("secret_sauce")
        driver.find_element(By.NAME, "login-button").click()
        time.sleep(1)

        """Find all elements in catalog"""
        print()
        print("ID ELEMENTS:")
        elem_id = driver.find_elements(By.XPATH, "//*[@id]")
        for x in elem_id:
            print(x.tag_name, x.get_attribute("id"))

        print()
        print("NAME ELEMENTS:")
        elem_name = driver.find_elements(By.XPATH, "//*[@name]")
        for x in elem_name:
            print(x.tag_name, x.get_attribute("name"))

        print()
        print("CLASS NAME ELEMENTS:")
        elem_class = driver.find_elements(By.XPATH, "//*[@class]")
        for x in elem_class:
            print(x.tag_name, x.get_attribute("class"))

    def test_log_output(self):
        """Output in log name and price of good"""
        driver = webdriver.Chrome("/home/natasha/PycharmProjects/Jenkins/chromedriver", chrome_options=chrome_options)
        driver.get("https://www.saucedemo.com")

        driver.find_element(By.ID, "user-name").send_keys("standard_user")
        driver.find_element(By.NAME, "password").send_keys("secret_sauce")
        driver.find_element(By.NAME, "login-button").click()
        time.sleep(1)

        good = driver.find_element(By.LINK_TEXT, "Sauce Labs Bolt T-Shirt")
        price = driver.find_element(By.XPATH, "//*[@id='inventory_container']/div/div[3]/div[2]/div[2]/div")

        logger = logging.getLogger("main")
        logger.setLevel(logging.DEBUG)
        file_handler = logging.FileHandler("../GoodInfo", mode="a")  # Create file with name and price of good
        logger.addHandler(file_handler)
        logger.info = "Good:{},price:{}".format(good.text, price.text)
